package password;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PasswordValidatorTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

//	Testing for the length of the password.
	@Test
	public void testCheckPasswordLength() {
		String psw = "thisisavalidpassword";
		System.out.println(psw);
		assertTrue("Invalid password length", PasswordValidator.checkPasswordLength(psw) == true);
		
	}
	@Test
	public void testCheckPasswordLengthException() {
		String psw = "pass";
		System.out.println(psw);
	}
	@Test
	public void testCheckPasswordLengthBoundaryIn() {
		String psw = "password";
		System.out.println(psw);
		assertTrue("Invalid password length", PasswordValidator.checkPasswordLength(psw) == true);
	}
	@Test
	public void testCheckPasswordLengthBoundaryOut() {
		String psw = "passwrd";
		System.out.println(psw);
	}
	
//	Testing for the digits of the password.
	@Test
	public void testCheckPasswordDigit() {
		String psw = "password123";
		System.out.println(psw);
		assertTrue("Must have atleast two digits", PasswordValidator.checkPasswordDigit(psw) == true);
	}
	@Test
	public void testCheckPasswordDigitException() {
		String psw = "password";
		System.out.println(psw);
	}
	@Test
	public void testCheckPasswordDigitBoundaryIn() {
		String psw = "password12";
		System.out.println(psw);
		assertTrue("Must have atleast two digits", PasswordValidator.checkPasswordDigit(psw) == true);
	}
	@Test
	public void testCheckPasswordDigitBoundaryOut() {
		String psw = "password1";
		System.out.println(psw);
	}

}
