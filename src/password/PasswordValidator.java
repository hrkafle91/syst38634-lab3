package password;

public class PasswordValidator {
	public static void main(String[] args) {
		
	}
	
	public static boolean checkPasswordLength(String password) {
		if(password.length() >= 8) { return true; } 
		else { return false; }
	}
	
	public static boolean checkPasswordDigit(String password) {
		int countDigit = 0;
		for(int i=0; i<password.length(); i++) {
			char currentChar = password.charAt(i);
			if(Character.isDigit(currentChar)) {
				countDigit++;
			}
		}
	    if(countDigit >= 2) {return true;}
	    else {return false;}
	}

}
